import * as titanrig from './sites/titanrig.js'
import * as amazon from './sites/amazon.js'
import mailer from "./mailer.js"
import * as performancepcs from './sites/performance-pcs-com.js'
import * as newegg from "./sites/newegg.js";
import * as frozencpu from "./sites/frozencpu-com.js";
import * as wallmart from "./sites/wallmart-com.js";
import * as corsair from "./sites/corsair-com.js";
import * as bestbuy from "./sites/bestbuy-com.js";
import * as evga from "./sites/evga-com.js";
import * as koolance from "./sites/koolance-com.js";

const CHECK_ITEM_INTERVAL = 5 // In minutes

const items = [
    {
        name: 'XSPC TX240 Ultra Thin Radiator',
        site: 'TitanRig',
        url: "https://www.titanrig.com/xspc-tx240-ultra-thin-radiator-120mm-x-2-dual-fan-0330xs014500xx.html#93=158",
        active: true,
        isInStockFn: titanrig.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'XSPC TX240 Ultra Thin Radiator',
        site: 'Amazon',
        url: "https://www.amazon.com/dp/B07FNXCLCJ/?coliid=I3DC3LOGK71V19&colid=1ZKF9WR4PXK9C&psc=0&ref_=lv_ov_lig_dp_it",
        active: true,
        isInStockFn: amazon.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'XSPC TX240 Ultra Thin Radiator',
        site: 'Performance PCS',
        url: "https://www.performance-pcs.com/water-cooling/radiators/xspc-tx240-ultrathin-radiator-xspc-tx240-bk.html",
        active: true,
        isInStockFn: performancepcs.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'XSPC TX240 Ultra Thin Radiator',
        site: 'Newegg',
        url: "https://www.newegg.com/p/2YM-000M-00126",
        active: true,
        isInStockFn: newegg.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'XSPC TX240 Ultra Thin Radiator',
        site: 'FrozenCPU',
        url: "https://www.frozencpu.com/products/10001802/ex-rad-789/XSPC_TX240_Ultrathin_Radiator.html?tl=g59c673s2154",
        active: true,
        isInStockFn: frozencpu.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'XSPC TX240 Ultra Thin Radiator',
        site: 'Wallmart',
        url: "https://www.walmart.com/ip/xspc-tx240-ultra-thin-radiator-120mm-x-2-dual-fan-black/224431718",
        active: true,
        isInStockFn: wallmart.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'SF Series™ SF750 — 750 Watt 80 PLUS® Platinum Certified',
        site: 'Corsair',
        url: "https://www.corsair.com/us/en/Categories/Products/Power-Supply-Units/Power-Supply-Units-Advanced/SF-Series/p/CP-9020186-NA",
        active: true,
        isInStockFn: corsair.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'NVIDIA GeForce RTX 3080 FE',
        site: 'Bestbuy',
        url: "https://www.bestbuy.com/site/nvidia-geforce-rtx-3080-10gb-gddr6x-pci-express-4-0-graphics-card-titanium-and-black/6429440.p?skuId=6429440",
        active: true,
        isInStockFn: bestbuy.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3 Ultra',
        site: 'Bestbuy',
        url: "https://www.bestbuy.com/site/evga-geforce-rtx-3080-xc3-ultra-gaming-10gb-gddr6-pci-express-4-0-graphics-card/6432400.p?skuId=6432400",
        active: true,
        isInStockFn: bestbuy.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3',
        site: 'Bestbuy',
        url: "https://www.bestbuy.com/site/evga-geforce-rtx-3080-xc3-gaming-10gb-gddr6-pci-express-4-0-graphics-card/6436194.p?skuId=6436194",
        active: true,
        isInStockFn: bestbuy.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3 Black',
        site: 'Bestbuy',
        url: "https://www.bestbuy.com/site/evga-geforce-rtx-3080-xc3-black-gaming-10gb-gddr6-pci-express-4-0-graphics-card/6432399.p?skuId=6432399",
        active: true,
        isInStockFn: bestbuy.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3 Ultra',
        site: 'Evga',
        url: "https://www.evga.com/products/product.aspx?pn=10G-P5-3885-KR",
        active: true,
        isInStockFn: evga.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3',
        site: 'Evga',
        url: "https://www.evga.com/products/product.aspx?pn=10G-P5-3883-KR",
        active: true,
        isInStockFn: evga.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3 Black',
        site: 'Evga',
        url: "https://www.evga.com/products/product.aspx?pn=10G-P5-3881-KR",
        active: true,
        isInStockFn: evga.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3 Ultra',
        site: 'Newegg',
        url: "https://www.newegg.com/evga-geforce-rtx-3080-10g-p5-3885-kr/p/N82E16814487520",
        active: true,
        isInStockFn: newegg.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3 Black',
        site: 'Newegg',
        url: "https://www.newegg.com/evga-geforce-rtx-3080-10g-p5-3885-kr/p/N82E16814487522",
        active: true,
        isInStockFn: newegg.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    {
        name: 'EVGA - GeForce RTX 3080 XC3',
        site: 'Newegg',
        url: "https://www.newegg.com/evga-geforce-rtx-3080-10g-p5-3883-kr/p/N82E16814487521",
        active: true,
        isInStockFn: newegg.isInStock,
        prevIsInStock: false,
        prevError: null,
        alertRecipients: [process.env.MAILER_TARGET]
    },
    // {
    //     name: 'Fitting Single, *Black* Adjustable Elbow (Low Profile), G 1/4 BSPP',
    //     site: 'Koolance',
    //     url: "https://koolance.com/fitting-single-black-swiveling-elbow-low-profile",
    //     active: true,
    //     isInStockFn: koolance.isInStock,
    //     prevIsInStock: false,
    //     prevError: null,
    //     alertRecipients: [process.env.MAILER_TARGET]
    // },
]

const checkItems = async (items) => {
    console.log(`[${new Date().toISOString()}] Checking items...`)
    const start = new Date().getTime();
    await Promise.all(items.map(async item => {
        try {
            const isInStock = await item.isInStockFn(item.url)
            console.log(`[${new Date().toISOString()}] [\x1b[33m${item.site}\x1b[0m] ${item.name}: ${isInStock ? `\x1b[32m${isInStock}\x1b[0m` : `\x1b[31m${isInStock}\x1b[0m`} (\x1b[36m${(new Date().getTime() - start) / 1000}s\x1b[0m)`)
            if (isInStock !== item.prevIsInStock) {
                item.prevIsInStock = isInStock
                const subject = `${isInStock ? 'IN-STOCK' : 'OUT-OF-STOCK'} - [${item.site}] ${item.name}`
                const message = `<p>${item.name} ${isInStock ? 'is in stock!' : 'is no longer in stock.'}</p><p><a href="${item.url}">${item.url}</a></p>`
                item.alertRecipients.forEach((recipient) => mailer.send(recipient, subject, message))
            }
        } catch (err) {
            console.error(`[${new Date().toISOString()}] ${err}`)
        }
    }))
    console.log(`[${new Date().toISOString()}] Total Time: ${(new Date().getTime() - start) / 1000}s`)
}
const checkItemsInterval = (interval) => {
    setTimeout(() => {
        checkItems(items)
        checkItemsInterval(CHECK_ITEM_INTERVAL)
    }, interval * 60 * 1000 + (Math.random() * 5))
}

const main = async () => {
    checkItems(items)
    checkItemsInterval(CHECK_ITEM_INTERVAL)
}

main()


