import nodemailer from "nodemailer";
import dotenv from 'dotenv'
dotenv.config()

class Mailer {
    transporter;
    baseSubject = 'Stock\'d Status Update';
    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: process.env.MAILER_USERNAME,
                pass: process.env.MAILER_PASSWORD,
            },
        });
    }
    send = async (to, subject, message) => {
        try {
            const mailerOptions = {
                from: process.env.MAILER_USERNAME,
                to,
                subject: `${this.baseSubject} - ${subject}`,
                html: message
            }
            await this.transporter.sendMail(mailerOptions, (err, info) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log('email sent to ', process.env.MAILER_TARGET)
                }
            })
        } catch (err) {
            console.log(err)
        }
    }
}

export default new Mailer()
