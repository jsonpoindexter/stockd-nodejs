import * as jsdom from 'jsdom'
import axios from "axios";

const { JSDOM } = jsdom;

export const isInStock = async (url) => {
    const {data} = await axios.get(url)
    const {document} = (new JSDOM(data)).window;
    return !document.querySelector('.pstock-out');
}
