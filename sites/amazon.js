import * as jsdom from 'jsdom'
import axios from "axios";
const { JSDOM } = jsdom;

export const isInStock = async (url) => {
    const {data} = await axios.get(url, {
        headers: {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0'},
    })
    const {document} = (new JSDOM(data)).window;
    const parent = document.getElementById('availability')
    if (!parent) return "Unable to determine stock status"
    const stockText = parent.querySelector('span').textContent.trim()
    // Options:
    // In stock
    // Out of stock
    // Low Stock (Only X left)
    return !stockText.match("Currently unavailable.");
}
