import * as jsdom from 'jsdom'
import axios from "axios";
const { JSDOM } = jsdom;

export const isInStock = async (url) => {
    const {data} = await axios.get(url)
    const {document} = (new JSDOM(data)).window;
    const parent = document.querySelector('.fulfillment-add-to-cart-button')
    if (!parent) return "Unable to determine stock status"
    const stockText = parent.querySelector('button').textContent
   switch(stockText) {
       case "Sold Out": return false
       default: {
           console.log(stockText)
           return true
       }
   }
}
