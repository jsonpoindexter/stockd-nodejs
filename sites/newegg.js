import * as jsdom from 'jsdom'
import axios from "axios";
const { JSDOM } = jsdom;

export const isInStock = async (url) => {
    const {data} = await axios.get(url)
    const {document} = (new JSDOM(data)).window;
    const parent = document.querySelector('.product-inventory')
    if (!parent) throw "Unable to determine stock status"
    const stockText = parent.children[0].textContent.trim()
    switch(stockText) {
        case "OUT OF STOCK.": return false
        default: return true
    }
}
