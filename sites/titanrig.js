import * as jsdom from 'jsdom'
import axios from "axios";
const { JSDOM } = jsdom;

export const isInStock = async (url) => {
    const {data} = await axios.get(url)
    const {document} = (new JSDOM(data)).window;
    const parent = document.getElementById('pro-stock-status')
    if (!parent) return "Unable to determine stock status"
    const stockText = parent.querySelector('span').textContent
    // Options:
    // In stock
    // Out of stock
    // Low Stock (Only X left)
    switch(stockText) {
        case "Out of stock": return false
        default: return true
    }
}
