import * as jsdom from 'jsdom'
import axios from "axios";
const { JSDOM } = jsdom;

export const isInStock = async (url) => {
    const {data} = await axios.get(url)
    const {document} = (new JSDOM(data)).window;
    const stockText = document.querySelector('#stock_text').textContent.trim()
    switch(stockText) {
       case "Out Of Stock": return false
       default: return true
    }
}
