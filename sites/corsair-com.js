import * as jsdom from 'jsdom'
import axios from "axios";
const { JSDOM } = jsdom;

export const isInStock = async (url) => {
    const {data} = await axios.get(url)
    const {document} = (new JSDOM(data)).window;
    const parent = document.getElementById('addToCartForm')
    if (!parent) return "Unable to determine stock status"
    return !parent.querySelector('.text-out-of-stock')
}
